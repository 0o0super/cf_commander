import { EventEmitter } from "ws";

export interface ICommander {
  registerConnection(socket: EventEmitter): void;
  commandLoop(config: RoundConfig): Promise<RoundConfig>;
}

export type RoundConfig = {
  round: number;
  nodes: number;
  provider: string;
};

export enum CFMessageTypes {
  INFO = "info",
  INTRODUTION = "intro",
  OFFER_PEERS = "offer_peers",
  INITIALIZED = "initialized",
  ALL_INI = "allInitialized",
  BASE_OK = "baseNetworkCompleted",
  ALL_BASE_OK = "allBaseNetworkCompleted",
  FINISHED = "finished",
  FORCE_STOP = "force_stop",
  ROUND_END = "round_end"
}

export type CFMessage = {
  id: string;
  type: CFMessageTypes;
  payload: any;
};

export type IntroductionPayload = {
  id: string;
  cfId: string;
  addr: string[];
};

export type OfferPeerPayload = {
  p2pPeers: { id: string; addr: string[] }[];
  cfAddrs: string[];
  channelMap: { [addr: string]: string[] };
  minimumGraph: string[];
};

export type FinishReportPayload = {
  onChainTxCount: number;
  installStats: { length: number; time: number }[];
  uninstallStats: { length: number; time: number }[];
};
