import { Contract, ContractFactory, Wallet } from "ethers";
import { AddressZero } from "ethers/constants";

import AppRegistryApp from "./contracts/AppRegistry.json";
import BalanceRefundApp from "./contracts/ETHBalanceRefundApp.json";
import ETHBucketApp from "./contracts/ETHBucket.json";
import MinimumViableMultisig from "./contracts/MinimumViableMultisig.json";
import NonceRegistryApp from "./contracts/NonceRegistry.json";
import ProxyFactory from "./contracts/ProxyFactory.json";
import TicTacToeApp from "./contracts/TicTacToeApp.json";

export default async function configureNetworkContext(wallet: Wallet) {
  const promiseToWait: Promise<Contract>[] = [];

  const balanceRefundContract = await new ContractFactory(
    BalanceRefundApp.abi,
    BalanceRefundApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(balanceRefundContract.deployed());

  const mvmContract = await new ContractFactory(
    MinimumViableMultisig.abi,
    MinimumViableMultisig.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(mvmContract.deployed());

  const proxyFactoryContract = await new ContractFactory(
    ProxyFactory.abi,
    ProxyFactory.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(proxyFactoryContract.deployed());

  const ethBucketContract = await new ContractFactory(
    ETHBucketApp.abi,
    ETHBucketApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(ethBucketContract.deployed());

  const appRegistryContract = await new ContractFactory(
    AppRegistryApp.interface,
    AppRegistryApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(appRegistryContract.deployed());

  const tttContract = await new ContractFactory(
    TicTacToeApp.interface,
    TicTacToeApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(tttContract.deployed());

  const nonceRegistryContract = await new ContractFactory(
    NonceRegistryApp.interface,
    NonceRegistryApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(nonceRegistryContract.deployed());

  await Promise.all(promiseToWait);

  return {
    AppRegistry: appRegistryContract.address,
    NonceRegistry: nonceRegistryContract.address,
    ETHBalanceRefundApp: balanceRefundContract.address,
    ETHBucket: ethBucketContract.address,
    MinimumViableMultisig: mvmContract.address,
    ProxyFactory: proxyFactoryContract.address,
    TicTacToe: tttContract.address,
    MultiSend: AddressZero,
    StateChannelTransaction: AddressZero,
    ETHVirtualAppAgreement: AddressZero
  };
}
