declare type Graph = Map<string, Set<string>>;
export default class ChannelGraph {
  private graph: Graph = new Map();

  constructor() {}

  importGraph(graph: { [addr: string]: string[] }) {
    // merge instead of replace all
    [...Object.entries(graph)].forEach(e => {
      let node: Set<string>;
      if (this.graph.has(e[0])) {
        node = this.graph.get(e[0])!;
      } else {
        node = new Set();
      }
      e[1].forEach(p => node.add(p));
    });
  }

  exportGraph() {
    const ret: { [addr: string]: string[] } = {};
    [...this.graph].sort().forEach(v => {
      ret[v[0]] = Array.from(v[1].keys()).sort();
    });
    return ret;
  }

  setConnection(xpub1: string, xpub2: string) {
    if (this.graph.has(xpub1) && xpub2) {
      const node = this.graph.get(xpub1)!;
      node.add(xpub2);
      // this.graph.set(xpub1, node); // not sure if needed
    } else if (!this.graph.has(xpub1)) {
      this.graph.set(xpub1, xpub2 ? new Set([xpub2]) : new Set());
    }

    if (this.graph.has(xpub2)) {
      const node = this.graph.get(xpub2)!;
      node.add(xpub1);
      // this.graph.set(xpub2, node); // not sure if needed
    } else {
      this.graph.set(xpub2, new Set([xpub1]));
    }
  }

  printGraph() {
    let result = "";
    this.graph.forEach((v, k) => {
      const str = Array.from(v.keys())
        .map(a => a.slice(6, 12))
        .join(", ");
      result += `[${k.slice(6, 12)}] ${str}\n`;
    });
    console.log(result);
  }

  getConnections(xpub: string): string[] {
    if (this.graph.has(xpub)) {
      return Array.from(this.graph.get(xpub)!.keys());
    }
    return [];
  }

  private breadFirstSearch(from: string, to: string): string[] {
    const predNode = new Map<string, string>([[from, ""]]);
    const queue = [from];
    while (!predNode.has(to) && queue.length > 0) {
      const start = queue.shift()!;
      const neibors = Array.from(this.graph.get(start)!.keys());
      neibors.forEach(n => {
        if (!predNode.has(n)) {
          queue.push(n);
          predNode.set(n, start);
        }
      });
    }
    if (!predNode.has(to)) {
      return [];
    }
    const path: string[] = [to];
    while (!path.includes(from)) {
      const pre = predNode.get(path[0])!;
      path.unshift(pre);
    }
    return path;
  }

  getIntermediariesBetween(from: string, to: string): string[] {
    const result = this.breadFirstSearch(from, to);
    return result;
  }
}

// const cg = new ChannelGraph();
// cg.setConnection("0", "1");
// // cg.setConnection("1", "2");
// cg.setConnection("2", "3");
// cg.setConnection("3", "4");
// cg.setConnection("4", "5");
// cg.setConnection("5", "6");
// // cg.setConnection("6", "7");
// cg.setConnection("7", "0");
// const whee = cg.getIntermediariesBetween("4", "0");
// console.log(whee);
