import Loki from "lokijs";
import Websocket from "ws";

import { ICommander, IntroductionPayload, RoundConfig } from "./types";

enum CFMessageTypes {
  INFO,
  INTRODUCTION,
  OFFER_PEERS,
  INITIALIZED,
  ALL_INI,
  DEPOSIT_DONE,
  ALL_DONE_DEPOSIT,
  ROUND_END,
  FINISHED
}

type CFMessage = {
  id: string;
  type: CFMessageTypes;
  payload: any;
};

type OfferPeerPayload = {
  p2pPeers: { id: string; addr: string[] }[];
  cfAddrs: string[];
};

type FinishReportPayload = {
  length: number;
  time: number;
}[];

export default class NewBroadcastCommander implements ICommander {
  private participantsWs = new Map<string, Websocket>();
  private introductionResolves: ((data: IntroductionPayload) => void)[] = [];
  private allInitializedResolves: (() => void)[] = [];
  private depositDoneResolves: Map<string, () => void> = new Map();
  private finishedResolves: (data: FinishReportPayload) => void = () => {};

  private resultStore: Loki;

  constructor() {
    this.resultStore = new Loki(`/home/gmin/broadcast${Date.now()}`);
    if (!this.resultStore.getCollection("result")) {
      this.resultStore.addCollection("result");
      this.resultStore.save();
    }
  }

  registerConnection(socket: Websocket): void {
    socket.on("message", async (message: string) => {
      const msg: CFMessage = JSON.parse(message);
      switch (msg.type) {
        case CFMessageTypes.INTRODUCTION:
          this.participantsWs.set(msg.id, socket);
          if (this.introductionResolves.length > 0) {
            this.introductionResolves.pop()!(
              msg.payload as IntroductionPayload
            );
          }
          break;
        case CFMessageTypes.INITIALIZED:
          this.allInitializedResolves.pop()!();
          break;
        case CFMessageTypes.DEPOSIT_DONE:
          const owners: string[] = msg.payload;
          const key = owners.sort().join("");
          this.depositDoneResolves.get(key)!();
          break;
        case CFMessageTypes.FINISHED:
          this.finishedResolves(msg.payload);
          break;
        case CFMessageTypes.INFO:
          console.log(`${JSON.stringify(msg.payload)}`);
          break;
      }
    });
  }

  async commandLoop(config: RoundConfig): Promise<RoundConfig> {
    // initialize storage...
    this.participantsWs.clear();
    this.introductionResolves = [];
    this.depositDoneResolves = new Map();
    this.finishedResolves = () => {};

    const introductionPromises: Promise<IntroductionPayload>[] = Array.apply(
      null,
      Array(config.nodes)
    ).map(() => new Promise(r => this.introductionResolves.push(r)));
    const allAddrs = await Promise.all(introductionPromises);
    const allCFAddrs = allAddrs.map(e => e.cfId);
    const allP2pPeers = allAddrs.map(e => {
      return {
        id: e.id,
        addr: e.addr
      };
    });
    console.log("All node offered their address!");

    const numNodesToSend = Math.ceil(allAddrs.length * 0.7);
    for (const ws of this.participantsWs.values()) {
      // send all peers info to nodes
      const payloadToSend: OfferPeerPayload = {
        p2pPeers: [...allP2pPeers]
          .sort(() => Math.random() - 0.5)
          .splice(0, numNodesToSend),
        cfAddrs: allCFAddrs
      };
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.OFFER_PEERS,
          payload: payloadToSend
        } as CFMessage)
      );
    }

    const allInitializedPromises: Promise<number>[] = allAddrs.map(
      () => new Promise(r => this.allInitializedResolves.push(r))
    );
    await Promise.all(allInitializedPromises);
    for (const ws of this.participantsWs.values()) {
      // tell everyone this round end
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.ALL_INI,
          payload: null
        } as CFMessage)
      );
    }

    const depositDonePromises: Promise<void>[] = [];
    for (let i = 0; i < allCFAddrs.length - 1; i += 1) {
      const sortedPair = [allCFAddrs[i], allCFAddrs[i + 1]].sort().join("");
      const promise: Promise<void> = new Promise(r =>
        this.depositDoneResolves.set(sortedPair, r)
      );
      depositDonePromises.push(promise);
    }
    await Promise.all(depositDonePromises);
    for (const ws of this.participantsWs.values()) {
      // tell everyone this round end
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.ALL_DONE_DEPOSIT,
          payload: null
        } as CFMessage)
      );
    }

    const finishData: FinishReportPayload = await new Promise(
      r => (this.finishedResolves = r)
    );
    console.log("All node done!");

    for (const ws of this.participantsWs.values()) {
      // tell everyone this round end
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.ROUND_END,
          payload: 1 // this sould be round number or next round config!!
        } as CFMessage)
      );
    }

    const resultString = finishData
      .map(a => `${a.length}@${a.time}`)
      .join(", ");
    // for now it is!!
    console.log(`
    -----Round ${config.round} Statistics-----
    Result: ${resultString}
    `);
    // save data to db!
    const col = this.resultStore.getCollection("result");
    col.insert(finishData);
    this.resultStore.save();

    return {
      ...config,
      round: config.round + 1
    };
  }
}
