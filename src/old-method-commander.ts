import { appendFileSync } from "fs";
import Queue from "p-queue";
import Websocket from "ws";

import ChannelGraph from "./ChannelGraph";
import { ICommander, RoundConfig } from "./types";

enum CFMessageTypes {
  INFO,
  INITIALIZED,
  DEPOSIT_DONE,
  DONE_INSTALL_VIRTUAL,
  DONE_UNINSTALL_VIRTUAL,
  FINISHED
}

type CFMessage = {
  id: string;
  type: CFMessageTypes;
  payload: any;
};

export default class OldMethodCommander implements ICommander {
  private participantsWs = new Map<string, Websocket>();
  private q = new Queue({ concurrency: 1 });
  private channelGraph = new ChannelGraph();
  private initializedResolves: ((id: string) => void)[] = [];
  private depositDoneResolves: Map<
    string,
    (edge: string[]) => void
  > = new Map();
  private virtualChannelResolves: Map<
    string,
    (msg: CFMessage) => void
  > = new Map();
  private finishedResolves: Map<string, () => void> = new Map();

  constructor() {}

  registerConnection(socket: Websocket): void {
    socket.on("message", async (message: string) => {
      const msg: CFMessage = JSON.parse(message);
      switch (msg.type) {
        case CFMessageTypes.INITIALIZED:
          this.participantsWs.set(msg.id, socket);
          if (this.initializedResolves.length > 0) {
            this.initializedResolves.pop()!(msg.id);
          }
          break;
        case CFMessageTypes.DEPOSIT_DONE:
          const owners: string[] = msg.payload;
          await this.q.add(async () => {
            this.channelGraph.setConnection(owners[0], owners[1]);
            // channelGraph.getIntermediariesBetween()
            const key = owners.sort().join("");
            let counter = 8;
            while (counter > 0) {
              try {
                this.depositDoneResolves.get(key)!(owners);
                break;
              } catch (error) {
                console.error(key);
                counter -= 1;
                await new Promise(r => setTimeout(r, 500));
              }
            }
          });
          break;
        case CFMessageTypes.DONE_INSTALL_VIRTUAL:
        case CFMessageTypes.DONE_UNINSTALL_VIRTUAL:
          const key = `${msg.id}${msg.type}`;
          if (this.virtualChannelResolves.has(key)) {
            this.virtualChannelResolves.get(key)!(msg);
          }
          break;
        case CFMessageTypes.FINISHED:
          // console.log(`${msg.id} DONE`);
          // participants.delete(msg.id);
          // if (participants.size === 0) {
          //   console.log("ALL OF THEM DONE");
          // }
          this.finishedResolves.get(msg.id)!();
          break;
        case CFMessageTypes.INFO:
          console.log(`${msg.payload}`);
          break;
      }
    });
  }

  async commandLoop(config: RoundConfig): Promise<RoundConfig> {
    // initialize storage...
    this.participantsWs.clear();
    this.initializedResolves = [];
    this.depositDoneResolves = new Map();
    this.finishedResolves = new Map();
    this.virtualChannelResolves = new Map();

    const initializedPromises: Promise<string>[] = Array.apply(
      null,
      Array(config.nodes)
    ).map(() => new Promise(r => this.initializedResolves.push(r)));
    const allParticipants = await Promise.all(initializedPromises);
    console.log("All node initialized!");

    const sortedXpub = allParticipants.sort();
    const depositDonePromises: Promise<any>[] = [];
    for (let i = 0; i < sortedXpub.length - 1; i += 1) {
      // const diff = i + 1 === sortedXpub.length ? -1 : +1;
      const sortedPair = [sortedXpub[i], sortedXpub[i + 1]].sort().join("");
      depositDonePromises.push(
        new Promise(r => this.depositDoneResolves.set(sortedPair, r))
      );
    }

    await Promise.all(depositDonePromises);
    console.log("All node done deposit!");

    const firstNode = sortedXpub[0];
    for (const target of sortedXpub) {
      const participants = this.channelGraph.getIntermediariesBetween(
        firstNode,
        target
      );
      if (participants.length > 2) {
        const virtualChannelPromises: Promise<CFMessage>[] = [
          `${firstNode}${CFMessageTypes.DONE_UNINSTALL_VIRTUAL}`,
          `${target}${CFMessageTypes.DONE_INSTALL_VIRTUAL}`
        ].map(k => new Promise(r => this.virtualChannelResolves.set(k, r)));

        this.participantsWs.get(firstNode)!.send(
          JSON.stringify({
            type: "proposeInstallVirtual",
            data: {
              from: firstNode,
              to: target,
              inters: participants.filter(a => a !== firstNode && a !== target)
            }
          })
        );

        const result = await Promise.all(virtualChannelPromises);
        this.virtualChannelResolves.clear();
        for (const r of result) {
          let type: string;
          if (r.type === CFMessageTypes.DONE_INSTALL_VIRTUAL) {
            type = "Install";
          } else {
            // uninstall
            type = "Unistall";
          }
          appendFileSync(
            "./exp_data.txt",
            `${type}, ${r.payload.channelLength}, ${r.payload.time}ms\n`
          );
        }
        await new Promise(r => setTimeout(r, 1000));
      }
    }

    [...this.participantsWs.values()].map(ws => {
      ws.send(
        JSON.stringify({
          type: "roundEnd",
          data: {}
        })
      );
    });

    const finishedPromises = allParticipants.map(
      a => new Promise(r => this.finishedResolves.set(a, r))
    );
    await Promise.all(finishedPromises);
    console.log("All nodes exit, begin new round..");

    return {
      ...config,
      round: config.round + 1
    };
  }
}
