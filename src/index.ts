import { WeiPerEther } from "ethers/constants";
import { JsonRpcProvider, TransactionRequest } from "ethers/providers";
import { BigNumber } from "ethers/utils";
import Koa from "koa";
import KoaBody from "koa-body";
// import logger from "koa-logger";
import Route from "koa-route";
import Router from "koa-router";
import websockify from "koa-websocket";
import Queue from "p-queue";

import AutoNonceWallet from "./auto-nonce-wallet";
// import ChannelGraph from "./ChannelGraph";
import configureNetworkContext from "./contractDeployment";
import NewBroadcastCommander from "./new-broadcast-commander";
import NewMethodCommander from "./new-method-commander";
import NewThroughputCommander from "./new-throughput-commander";
import OldMethodCommander from "./old-method-commander";
import { ICommander, RoundConfig } from "./types";
const argv = require("yargs").argv;

const app = websockify(new Koa());
const http = new Router();
const q = new Queue({ concurrency: 1 });

const FAUCET_PK =
  (argv.privateKey as string) ||
  "0x0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef";
const PROVIDER = (argv.provider as string) || "http://127.0.0.1:8545";
const NUM_NODES = argv.num ? parseInt(argv.num, 10) : 5;
const PEER_INFO_FRACTION = argv.peerInfoFraction
  ? parseFloat(argv.peerInfoFraction)
  : 0.5;
const ROUND_TIMEOUT = argv.roundTimeout
  ? parseInt(argv.roundTimeout, 10)
  : 120000;
const CMDER_TYPE = argv.commanderType ? argv.commanderType : "NewMethod";

// send some ether!
let provider: JsonRpcProvider;
// get some ether from faucet
let masterWallet: AutoNonceWallet;
let roundConfig: RoundConfig = {
  round: 1,
  nodes: NUM_NODES,
  provider: PROVIDER
};

// deply contracts!
let networkContext: any = {};
async function init() {
  while (true) {
    try {
      provider = new JsonRpcProvider(PROVIDER);
      masterWallet = new AutoNonceWallet(FAUCET_PK, provider);
      networkContext = await configureNetworkContext(masterWallet);
      console.log("Contracts successfully deployed.");
      break;
    } catch (e) {
      console.error(e);
      // console.error(`Initializing error, retrying...`);
      await new Promise(r => setTimeout(r, 2000));
    }
  }

  // instainiate commander
  let cmder: ICommander;
  switch (CMDER_TYPE) {
    case "NewMethod":
      cmder = new NewMethodCommander(PEER_INFO_FRACTION, ROUND_TIMEOUT);
      break;
    case "OldMethod":
      cmder = new OldMethodCommander();
      break;
    case "NewThroughput":
      cmder = new NewThroughputCommander();
      break;
    case "NewBroadcast":
      cmder = new NewBroadcastCommander();
      break;
    default:
      cmder = new NewMethodCommander(PEER_INFO_FRACTION, ROUND_TIMEOUT);
      break;
  }

  http.get("/networkContext", async (ctx, next) => {
    ctx.body = networkContext;

    await next();
  });

  http.get("/currentRoundConfig", async (ctx, next) => {
    ctx.body = roundConfig;

    await next();
  });

  http.get("/faucet/:address", async (ctx, next) => {
    const requestAddr: string = ctx.params.address;
    const res = await q.add(async () => {
      const tx: TransactionRequest = {
        // nonce,
        gasLimit: 21000,
        gasPrice: new BigNumber("20000000000"),
        to: requestAddr,
        value: WeiPerEther.mul(1),
        data: "0x"
      };
      // nonce += 1;

      const txRes = await masterWallet.sendTransaction(tx);
      return txRes;
    });

    ctx.body = { address: requestAddr, tx: res.hash! };

    await next();
  });

  app.ws.use(
    Route.all("/socket", ctx => {
      cmder.registerConnection(ctx.websocket);

      /**
       * Let's design some flow!!!
       * Step1: make sure everyone initialized their node
       * Step2: I think they can create channel and deposit on their own
       * Step3: Nodes report multisig address that done deposit
       * Step4: Commander find a path and tell worker to install virtual
       * Step5: Uninstall virtual after installed
       * Step6: Nodes report execution time
       * Step7: Commander remove done nodes
       * Wtep8: Go to step4 and loop, until no nodes!
       */
    })
  );

  // Routes
  app.use(KoaBody());
  app.use(http.routes()).use(http.allowedMethods());
  // app.use(logger());

  app.listen(3000, () => {
    console.log("Koa started");
  });

  // start commander loop
  while (true) {
    console.log(`Round ${roundConfig.round} start!`);
    roundConfig = await cmder.commandLoop(roundConfig);
    console.log(`Round ${roundConfig.round - 1} end!`);
  }
}
init();
