import Loki from "lokijs";
import Websocket from "ws";

import ChannelGraph from "./ChannelGraph";
import {
  CFMessage,
  CFMessageTypes,
  FinishReportPayload,
  ICommander,
  IntroductionPayload,
  OfferPeerPayload,
  RoundConfig
} from "./types";

export default class NewMethodCommander implements ICommander {
  private participantsWs = new Map<string, Websocket>();
  private introductionResolves: ((data: IntroductionPayload) => void)[] = [];
  private allInitializedResolves: (() => void)[] = [];
  private baseNetworkResolved: (() => void)[] = [];
  private finishedResolves: ((data: FinishReportPayload) => void)[] = [];
  private forceStopResolves!: (data: string) => void;

  private resultStore: Loki;

  constructor(private peerFraction: number, private timeout: number) {
    this.resultStore = new Loki(`/home/gmin/newMethod-${Date.now()}`);
    if (!this.resultStore.getCollection("result")) {
      this.resultStore.addCollection("result");
      this.resultStore.save();
    }
  }

  registerConnection(socket: Websocket): void {
    socket.on("message", async (message: string) => {
      const msg: CFMessage = JSON.parse(message);
      switch (msg.type) {
        case CFMessageTypes.INTRODUTION:
          this.participantsWs.set(msg.id, socket);
          if (this.introductionResolves.length > 0) {
            this.introductionResolves.pop()!(
              msg.payload as IntroductionPayload
            );
          }
          break;
        case CFMessageTypes.INITIALIZED:
          this.allInitializedResolves.pop()!();
          break;
        case CFMessageTypes.BASE_OK:
          this.baseNetworkResolved.pop()!();
          break;
        case CFMessageTypes.FINISHED:
          this.finishedResolves.pop()!(msg.payload);
          break;
        case CFMessageTypes.INFO:
          // disable for now!
          this.forceStopResolves(msg.payload);
          console.log(`${msg.payload}`);
          break;
      }
    });
  }

  private randomChannelMap(
    ratio: number,
    participants: string[]
  ): [ChannelGraph, string[], number, number] {
    const channelMap = new ChannelGraph();
    const allPossibleEdges = participants.reduce((p, c, i, a) => {
      [...a].splice(i + 1).forEach(e => p.add([e, c].sort().join(",")));
      return p;
    }, new Set<string>());
    // construct minimum graph
    const randomOrder = [...participants].sort(() => Math.random() - 0.5);
    for (let i = 0; i < randomOrder.length - 1; i += 1) {
      const edgeString = [randomOrder[i], randomOrder[i + 1]].sort().join(",");
      channelMap.setConnection(randomOrder[i], randomOrder[i + 1]);
      allPossibleEdges.delete(edgeString);
    }

    const len = participants.length;
    let connectionCounter = len - 1;

    [...allPossibleEdges.values()]
      .filter(() => Math.random() < ratio)
      .forEach(e => {
        const [xpub1, xpub2] = e.split(",");
        channelMap.setConnection(xpub1, xpub2);
        connectionCounter += 1;
      });

    // channelMap.printGraph(); // for debug
    // return map, random appear time, ledger channel count, virtual channel count
    return [
      channelMap,
      randomOrder,
      connectionCounter,
      (len * (len - 1)) / 2 - connectionCounter
    ];
  }

  async commandLoop(config: RoundConfig): Promise<RoundConfig> {
    // initialize storage...
    this.participantsWs.clear();
    this.introductionResolves = [];
    this.allInitializedResolves = [];
    this.baseNetworkResolved = [];
    this.finishedResolves = [];

    const introductionPromises: Promise<IntroductionPayload>[] = Array.apply(
      null,
      Array(config.nodes)
    ).map(() => new Promise(r => this.introductionResolves.push(r)));
    const allAddrs = await Promise.all(introductionPromises);
    const allCFAddrs = allAddrs.map(e => e.cfId);
    const allP2pPeers = allAddrs.map(e => {
      return {
        id: e.id,
        addr: e.addr
      };
    });
    // 0.0 ~ 1.0
    const pForThisRound = (config.round % 11) / 10;
    const [
      channelMap,
      randomOrder,
      ledgerChannelCnt,
      virtualChannelCnt
    ] = this.randomChannelMap(pForThisRound, allCFAddrs);
    // channelMap.printGraph();
    console.log("All node offered their address!");

    let timeoutInstance: NodeJS.Timeout | undefined;
    let forceStopReason: string | undefined;
    // force stop if timeout or error!
    Promise.race([
      new Promise(r => (this.forceStopResolves = r)),
      new Promise(
        r =>
          (timeoutInstance = setTimeout(
            () => r(`timeout from round ${config.round}`),
            this.timeout
          ))
      )
    ]).then((reason: any) => {
      forceStopReason = reason;
      for (const ws of this.participantsWs.values()) {
        // tell everyone force stop this round
        ws.send(
          JSON.stringify({
            id: "commander",
            type: CFMessageTypes.FORCE_STOP,
            payload: {
              reason: forceStopReason
            }
          } as CFMessage)
        );
      }
    });

    const numNodesToSend = Math.ceil(allAddrs.length * this.peerFraction);
    for (const ws of this.participantsWs.values()) {
      // send all peers info to nodes
      const payloadToSend: OfferPeerPayload = {
        p2pPeers: [...allP2pPeers]
          .sort(() => Math.random() - 0.5)
          .splice(0, numNodesToSend),
        cfAddrs: allCFAddrs,
        channelMap: channelMap.exportGraph(),
        minimumGraph: randomOrder
      };
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.OFFER_PEERS,
          payload: payloadToSend
        } as CFMessage)
      );
    }

    const allInitializedPromises: Promise<number>[] = allAddrs.map(
      () => new Promise(r => this.allInitializedResolves.push(r))
    );
    await Promise.all(allInitializedPromises);
    for (const ws of this.participantsWs.values()) {
      // tell everyone initialized
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.ALL_INI,
          payload: null
        } as CFMessage)
      );
    }

    const baseNetworkPromises: Promise<void>[] = allAddrs.map(
      () => new Promise(r => this.baseNetworkResolved.push(r))
    );
    await Promise.all(baseNetworkPromises);
    console.log("Base network constructed!!");

    // round start AFTER minimum network constructed!
    const roundStartTime = Date.now();
    for (const ws of this.participantsWs.values()) {
      // tell everyone this round end
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.ALL_BASE_OK,
          payload: null
        } as CFMessage)
      );
    }

    const finishedPromises: Promise<FinishReportPayload>[] = allAddrs.map(
      () => new Promise(r => this.finishedResolves.push(r))
    );

    const finishData = await Promise.all(finishedPromises);
    const totalTxCount = finishData
      .map(d => d.onChainTxCount)
      .reduce((p, c) => p + c, 0);
    const avgInstallTime = finishData
      .map(d => d.installStats)
      .reduce((p, c) => p.concat(c), [])
      .reduce((p, c, i, a) => p + c.time / a.length, 0);
    const avgUninstallTime = finishData
      .map(d => d.uninstallStats)
      .reduce((p, c) => p.concat(c), [])
      .reduce((p, c, i, a) => p + c.time / a.length, 0);
    console.log("All node done!");

    for (const ws of this.participantsWs.values()) {
      // tell everyone this round end
      ws.send(
        JSON.stringify({
          id: "commander",
          type: CFMessageTypes.ROUND_END,
          payload: 1 // this sould be round number or next round config!!
        } as CFMessage)
      );
    }

    // clear timeout so that it won't resolve in next round!!!!
    clearTimeout(timeoutInstance!);

    // for now it is!!
    const totalOffChainTxCount = (config.nodes * (config.nodes - 1) * 3) / 2;
    const elapsedTime = Date.now() - roundStartTime;
    console.log(`
    -----Round ${config.round} Statistics-----
    Toltal Tx sent on chain: ${totalTxCount}
    Total ledger channel count: ${ledgerChannelCnt}
    Total virtual channel count: ${virtualChannelCnt}
    Average install operation time: ${avgInstallTime}ms
    Average uninstall operation time: ${avgUninstallTime}ms
    Elapsed time: ${elapsedTime}ms
    Throughput: ${totalOffChainTxCount / (elapsedTime / 1000)} tx/s
    (More to come....)
    `);
    if (forceStopReason) {
      console.error(`This round finished by force stop!!
      Reason ${forceStopReason}`);
    } else {
      // save data to db!
      const col = this.resultStore.getCollection("result");
      col.insert({
        elapsedTime,
        ledgerChannelCnt,
        virtualChannelCnt,
        startTime: roundStartTime,
        minimumGraph: randomOrder,
        channelMap: channelMap.exportGraph(),
        resultData: finishData
      });
      this.resultStore.save();
    }

    return {
      ...config,
      round: config.round + 1
    };
  }
}
